let lightmodeButton = document.querySelector('#light');
let darkmodeButton = document.querySelector('#dark');
let app = document.querySelector('#app');
let colorSelector = document.querySelector('#colorSelector');
let text = document.querySelectorAll('.text');

let menu = document.querySelector('.menu')
menu.addEventListener('click', function () {
    colorSelector.style.left = '0'
})

let aleft = document.querySelector('.aleft');
aleft.addEventListener('click', function () {
    colorSelector.style.left = '-100vw'
})



// Color picker
let grey = document.querySelector('#grey');
let blue = document.querySelector('#blue');
let green = document.querySelector('#green');
let yellow = document.querySelector('#yellow');
let pink = document.querySelector('#pink');
let red = document.querySelector('#red');
let purple = document.querySelector('#purple');
let responsegrey = document.querySelector('#responsegrey');
let responseblue = document.querySelector('#responseblue');
let responsegreen = document.querySelector('#responsegreen');
let responseyellow = document.querySelector('#responseyellow');
let responsepink = document.querySelector('#responsepink');
let responsered = document.querySelector('#responsered');
let responsepurple = document.querySelector('#responsepurple');

grey.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#e3e3e3';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "black";
    } )
})
blue.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#2a73ee';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";

    } )
})
green.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#10a37f';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
yellow.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#FFD700FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
pink.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#FF69B4FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
red.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#c90808';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
purple.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#5436DA';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})

responsegrey.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#e3e3e3';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "black";
    } )
})
responseblue.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#2a73ee';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsegreen.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#10a37f';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    })
})
responseyellow.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#FFD700FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsepink.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#FF69B4FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsered.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#c90808';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsepurple.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#5436DA';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})









































lightmodeButton.addEventListener('click', function ()
{
    app.style.backgroundColor = '#eaeaea';
    colorSelector.style.backgroundColor = '#f1f1f1';
    text.forEach(element => element.style.color = '#343541')
})
darkmodeButton.addEventListener('click', function ()
{
    app.style.backgroundColor = '#343541';
    colorSelector.style.backgroundColor = '#525365'
    text.forEach(element => element.style.color = '#f1f1f1')
})