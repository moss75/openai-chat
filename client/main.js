import bot from './assets/bot.svg';
import user from './assets/user.svg';

const form = document.querySelector('form');
const chatContainer = document.querySelector('#chat_container');

let loadInterval;

function loader(element){
    element.textContent = '';

    loadInterval = setInterval(() => {
        element.textContent += '.';

        if(element.textContent === '....'){
            element.textContent ="";
        }
    }, 300)
}


function typeText(element, text) {
    let index = 0;

    let interval = setInterval(() => {
        if(index < text.length) {
            element.innerHTML += text.charAt(index);
            index++;
        } else {
            clearInterval(interval)
        }
    }, 20)
}

function generateUniqueId(){
    const timestamp = Date.now();
    const randomNumber = Math.random();
    const hexaDecimalString = randomNumber.toString(16)

    return `id-${timestamp}-${hexaDecimalString}`;
}

function chatStripe (isAi, value, uniqueId){
    return (
        `
            <div class="wrapper ${isAi && 'ai'}">
                <div class="chat">
                    <div class="profile">
                        <img src="${isAi ? bot : user}" alt="${isAi ? bot : user}"/>
                    </div>
                    <div class="message" id="${uniqueId}">${value}</div>
                </div>
            </div>
        `
    )
}

const handleSubmit = async (e) => {
    e.preventDefault();
    const data = new FormData(form)

    // generer la ligne utilisateur
    console.log(data.get('prompt'))
    console.log(chatContainer);
    chatContainer.innerHTML += chatStripe(false, data.get('prompt'));

    form.reset();


    // genere la ligne du bot
    const uniqueId = generateUniqueId();
    chatContainer.innerHTML += chatStripe(true, " ", uniqueId);
    chatContainer.scrollTop = chatContainer.scrollHeight;
    const messageDiv = document.getElementById(uniqueId);
    loader(messageDiv);

    //recuperer data du  server
    const response = await fetch('https://openai-chat-jn7z.onrender.com/', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            prompt: data.get('prompt')
        })
    })
    clearInterval(loadInterval);
    messageDiv.innerHTML = '';

    if (response.ok) {
        const data = await response.json();
        const parsedData = data.bot.trim();

        console.log(messageDiv);
        typeText(messageDiv, parsedData);
    } else {
        const err = await response.text();

        messageDiv.innerHTML += "erreur";

        alert(err);
    }
}

form.addEventListener('submit', handleSubmit);
form.addEventListener('keyup', (e) => {
    if (e.keyCode === 13){
        handleSubmit(e)
    }
})

// Color Change
let lightmodeButton = document.querySelector('#light');
let darkmodeButton = document.querySelector('#dark');
let app = document.querySelector('#app');
let colorSelector = document.querySelector('#colorSelector');
let text = document.querySelectorAll('.text');

let menu = document.querySelector('.menu')
menu.addEventListener('click', function () {
    colorSelector.style.left = '0'
})

let aleft = document.querySelector('.aleft');
aleft.addEventListener('click', function () {
    colorSelector.style.left = '-100vw'
})

// Color picker
let grey = document.querySelector('#grey');
let blue = document.querySelector('#blue');
let green = document.querySelector('#green');
let yellow = document.querySelector('#yellow');
let pink = document.querySelector('#pink');
let red = document.querySelector('#red');
let purple = document.querySelector('#purple');
let responsegrey = document.querySelector('#responsegrey');
let responseblue = document.querySelector('#responseblue');
let responsegreen = document.querySelector('#responsegreen');
let responseyellow = document.querySelector('#responseyellow');
let responsepink = document.querySelector('#responsepink');
let responsered = document.querySelector('#responsered');
let responsepurple = document.querySelector('#responsepurple');

grey.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#e3e3e3';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "black";
    } )
})
blue.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#2a73ee';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";

    } )
})
green.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#10a37f';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
yellow.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#FFD700FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
pink.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#FF69B4FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
red.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#c90808';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
purple.addEventListener('click', function (){
    let wrapper = document.querySelectorAll('.false');
    wrapper.forEach((element) => {
        element.style.backgroundColor = '#5436DA';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})

responsegrey.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#e3e3e3';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "black";
    } )
})
responseblue.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#2a73ee';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsegreen.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#10a37f';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    })
})
responseyellow.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#FFD700FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsepink.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#FF69B4FF';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsered.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#c90808';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})
responsepurple.addEventListener('click', function (){
    let ai = document.querySelectorAll('.ai');
    ai.forEach((element) => {
        element.style.backgroundColor = '#5436DA';
        let text = element.childNodes[1].lastChild.previousSibling;
        text.style.color = "#fff";
    } )
})

lightmodeButton.addEventListener('click', function ()
{
    app.style.backgroundColor = '#eaeaea';
    colorSelector.style.backgroundColor = '#f1f1f1';
    text.forEach(element => element.style.color = '#343541')
})
darkmodeButton.addEventListener('click', function ()
{
    app.style.backgroundColor = '#343541';
    colorSelector.style.backgroundColor = '#525365'
    text.forEach(element => element.style.color = '#f1f1f1')
})